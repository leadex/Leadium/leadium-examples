package allure

import com.codeborne.selenide.Condition
import io.qameta.allure.Allure
import io.qameta.allure.model.Status
import io.qameta.allure.util.ResultsUtils
import org.aspectj.lang.JoinPoint
import org.leadium.core.utils.replaceCamelCaseWithSpaces
import org.leadium.report.StepReceiver
import org.leadium.report.allure.startNewStep
import org.leadium.report.allure.updateStopStep
import org.leadium.report.event.LogEventReceiver
import org.leadium.report.event.data.LogEventData
import org.openqa.selenium.By
import org.openqa.selenium.Keys
import java.io.File
import java.util.*

/**
 * The object defines Listener of selenide log events for building Allure report.
 */
class ReportAdapter : LogEventReceiver, StepReceiver {

    override fun onReceive(logEventData: LogEventData) {
        val action = logEventData.action.replaceCamelCaseWithSpaces().toLowerCase().replace("_", " ")
        val value = if (logEventData.parameter != null) {
            when (val parameter = logEventData.parameter) {
                is String -> ": $parameter"
                is Short -> " seconds: $parameter"
                is Int -> " index: $parameter"
                is File -> " file: ${parameter.path}"
                is Condition -> " $parameter"
                is Long -> " $parameter ms."
                is By -> " by: $parameter"
                is Array<*> -> {
                    val sb = StringBuilder()
                    sb.append(" ")
                    parameter.forEach {
                        when (it) {
                            is CharSequence -> sb.append(it)
                            is Int -> sb.append(it).append(" ")
                            is File -> sb.append(it.path).append(" ")
                            is Condition -> sb.append(it).append(" ")
                            is By -> sb.append(it).append(" ")
                            is Keys -> sb.append(it.name).append(" ")
                        }
                    }
                    sb.toString()
                }
                else -> ""
            }
        } else ""
        val result = if (logEventData.result != null) {
            when (val result = logEventData.result) {
                is String -> " result: $result"
                is Boolean -> " result: $result"
                is File -> " result: ${result.path}"
                else -> ""
            }
        } else ""
        val label = logEventData.label
        val uuid = UUID.randomUUID().toString()
        if (label != null) {
            Allure.getLifecycle().startNewStep(uuid, "'$label' $action$value$result")
        } else {
            Allure.getLifecycle().startNewStep(uuid, "$action$value$result")
        }
        Allure.getLifecycle().updateStopStep(uuid, logEventData)
    }

    override fun before(joinPoint: JoinPoint?, uuid: String, name: String) {
        Allure.getLifecycle().startNewStep(uuid, name)
    }

    override fun afterReturning(joinPoint: JoinPoint?, uuid: String) {
        Allure.getLifecycle().updateStep(uuid) { s ->
            s.status = Status.PASSED
        }
    }

    override fun afterThrowing(joinPoint: JoinPoint?, uuid: String, e: Throwable) {
        Allure.getLifecycle().updateStep(uuid) { s ->
            s.status = ResultsUtils.getStatus(e).orElse(Status.BROKEN)
            s.statusDetails = ResultsUtils.getStatusDetails(e).orElse(null)
        }
    }

    override fun after(joinPoint: JoinPoint?, uuid: String) {
        Allure.getLifecycle().stopStep(uuid)
    }
}