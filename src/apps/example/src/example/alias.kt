package example

import org.apache.commons.lang3.tuple.Pair

typealias TestDataMap = LinkedHashMap<String, Pair<String, String>>