@file:Suppress("unused", "UNUSED_PARAMETER")

package example

import org.aspectj.lang.JoinPoint
import org.aspectj.lang.annotation.*
import org.leadium.report.AttachmentSyntax
import org.leadium.report.aop.AbstractReportSelenideStepAspect
import org.leadium.report.aop.AbstractReportStepAspect
import org.leadium.ui.VideoEncoder
import java.io.File

@Aspect
class ReportStepAspect : AbstractReportStepAspect() {

    @Pointcut("execution(* *..report.AbstractStep+.*(..))")
    fun step() {
        //
    }

    @Before("step()")
    fun before(jp: JoinPoint) {
        super.beforeStep(jp)
    }

    @AfterReturning("step()")
    fun afterReturning(jp: JoinPoint) {
        super.afterReturningStep(jp)
    }

    @AfterThrowing("step()", throwing = "e")
    fun afterThrowing(jp: JoinPoint, e: Throwable) {
        super.afterThrowingStep(jp, e)
    }

    @After("step()")
    fun after(jp: JoinPoint) {
        super.afterStep(jp)
    }
}

@Aspect
class SelenideAspect : AbstractReportSelenideStepAspect() {

    @Pointcut("call(* *..Selenide.*(..))")
    fun selenideMethod() {
        //
    }

    @Before("selenideMethod()")
    fun before(jp: JoinPoint) {
        super.beforeSelenideMethod(jp)
    }

    @AfterReturning("selenideMethod()", returning = "result")
    fun afterReturning(jp: JoinPoint, result: Any?) {
        super.afterReturningSelenideMethod(jp, result)
    }

    @AfterThrowing("selenideMethod()", throwing = "e")
    fun afterThrowing(jp: JoinPoint, e: Throwable) {
        super.afterThrowingSelenideMethod(jp, e)
    }

    @After("selenideMethod()")
    fun after(jp: JoinPoint) {
        super.afterSelenideMethod(jp)
    }
}

@Aspect
class VideoEncoderAspect : AttachmentSyntax {

    @Pointcut("execution(* *(..)) && @annotation(com.automation.remarks.junit5.Video)")
    fun withVideoAnnotation() {
//
    }

    @Pointcut("execution(* *(..)) && @annotation(org.junit.jupiter.api.AfterEach)")
    fun withAfterEachAnnotation() {
//
    }

    @After("withAfterEachAnnotation()")
    fun after(jp: JoinPoint) {
        attachVideo()
    }

    private fun attachVideo() {
            val encodedVideo = VideoEncoder().encodeToMP4(findVideo()!!)
            attachVideo(encodedVideo)
    }

    private fun findVideo(): File? {
        if (VideoProperties.enabled) {
            return File("${System.getProperty("rootProjectPath")}/src/apps/example/video")
                .walkTopDown().find { file ->
                    file.extension.contains("avi")
                }
        } else throw IllegalArgumentException("video.enabled=false")
    }
}