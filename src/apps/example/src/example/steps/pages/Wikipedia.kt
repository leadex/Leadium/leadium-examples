package example.steps.pages

import com.codeborne.selenide.Condition
import com.codeborne.selenide.Condition.*
import com.codeborne.selenide.Selenide.element
import org.junit.jupiter.api.Assertions.assertTrue
import org.leadium.report.AbstractStep
import org.leadium.report.aop.Step
import org.leadium.ui.XpathPresetSyntax
import org.openqa.selenium.By

class Wikipedia : AbstractStep, XpathPresetSyntax {

    @Step
    fun checkResult(expectedResult: String) {
        val element = element(By.xpath(xpathByText(expectedResult)))
        assertTrue(element.has(hidden))
    }

    @Step
    fun checkLogoTitle(logoTitle: String = "Visit the main page") {
        element(By.xpath("//a[@class='mw-wiki-logo']"))
            .should(have(attribute("title", logoTitle)))
    }

    @Step
    fun checkLogoCondition(condition: Condition) {
        element(By.xpath("//a[@class='mw-wiki-logo']"))
            .should(condition)
    }

    @Step
    fun checkSearchInput() {
        element(By.xpath("//input[@type='search']"))
            .should(be(enabled))
            .should(be(empty))
    }

    @Step
    fun mathematics() {
        element(By.xpath(xpathByText("mathematics")))
            .click()
    }

    @Step
    fun mainPage() {
        element(By.xpath(xpathByText("Main page")))
            .click()
    }
}