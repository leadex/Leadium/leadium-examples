package example.steps.pages

import com.codeborne.selenide.Condition.be
import com.codeborne.selenide.Condition.visible
import com.codeborne.selenide.Selenide.element
import org.leadium.report.AbstractStep
import org.leadium.report.aop.Step
import org.leadium.ui.XpathPresetSyntax
import org.openqa.selenium.By
import org.openqa.selenium.Keys

class DuckDuckGo : AbstractStep, XpathPresetSyntax {

    @Step
    fun setSearchBox(searchRequest: String) {
        element(By.xpath("//input[@id='search_form_input_homepage']"))
            .setValue(searchRequest)
    }

    @Step
    fun enter() {
        element(By.xpath("//input[@id='search_form_input_homepage']"))
            .sendKeys(Keys.ENTER)
    }

    @Step
    fun checkResult(expectedResult: String) {
        element(By.xpath(xpathByText(expectedResult, withText = true)))
            .should(be(visible))
    }

    @Step
    fun clickResult() {
        element(By.xpath("//*[text()='More at Wikipedia ']"))
            .click()
    }
}