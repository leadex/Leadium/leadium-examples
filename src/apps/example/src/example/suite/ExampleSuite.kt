package example.suite

import com.automation.remarks.junit5.Video
import example.cases.ExampleTest
import io.qameta.allure.Epic
import io.qameta.allure.Feature
import io.qameta.allure.Story
import org.junit.jupiter.api.AfterEach
import org.junit.jupiter.api.DisplayName
import org.junit.jupiter.api.DisplayNameGeneration
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith
import org.leadium.junit5.Inject
import org.leadium.junit5.PropertyTestData
import org.leadium.junit5.TestDataResolver
import org.leadium.junit5.TestDataType
import org.leadium.junit5.displayname.DisplayNameGenerator
import org.leadium.junit5.displayname.TestCaseDisplayName
import org.leadium.report.allure.description.DescriptionAppender
import java.io.IOException
import java.lang.System.getProperty

@Epic("EXAMPLE EPIC")
@DisplayName("Example Suite | positive")
@DisplayNameGeneration(DisplayNameGenerator::class)
@ExtendWith(TestDataResolver::class)
class ExampleSuite : TestSuite() {

    companion object {
        const val resourcesPath: String = "/src/apps/example/res"
    }

    @Test
    @Video
    @Feature("POSITIVE")
    @Story("EXAMPLE STORY")
    @TestCaseDisplayName(ExampleTest::class)
    @TestDataResolver.Settings(TestDataType.PROPERTY, resourcesPath, ["example.property"])
    fun wikiTest(@Inject testData: PropertyTestData) {
        val testCase = ExampleTest(testData.map)
        DescriptionAppender(getProperty("rootProjectPath"), getProperty("gitLabMasterUrl"))
            .appendLinkToTestCase(testCase)
            .appendPropertyFile(testData.file)
            .apply()
        testCase.begin()
    }

    @AfterEach
    override fun afterEach() {
        try {
            super.afterEach()
        } catch (e: IOException) {
            //
        }
    }
}