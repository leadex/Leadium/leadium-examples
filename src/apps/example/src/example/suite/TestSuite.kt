package example.suite

import allure.ReportAdapter
import com.codeborne.selenide.Configuration
import com.codeborne.selenide.Selenide
import example.AuthProperties
import org.apache.log4j.BasicConfigurator
import org.junit.jupiter.api.AfterEach
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.extension.RegisterExtension
import org.leadium.junit5.FailedTestsAggregator
import org.leadium.report.StepProvider
import org.leadium.report.allure.junit5.AllureAfterEach
import org.leadium.report.aop.AbstractReportSelenideStepAspect
import org.leadium.report.event.LogEventProvider
import org.leadium.ui.Selenoid
import java.io.File
import java.lang.System.getProperty

abstract class TestSuite {

    companion object {

        val allureReportAdapter = ReportAdapter()

        init {
            StepProvider.register(allureReportAdapter)

            LogEventProvider.register(allureReportAdapter)

            BasicConfigurator.configure()
//            Logger.getRootLogger().level = Level.OFF
        }
    }

    @JvmField
    @RegisterExtension
    var failedTestsAggregator = FailedTestsAggregator(getProperty("user.dir"))

    @JvmField
    @RegisterExtension
    var allure = AllureAfterEach(
        AuthProperties.baseurl,
        File("${getProperty("rootProjectPath")}/res/categories.json"),
        "${getProperty("user.dir")}/${getProperty("allure.results.directory", "build/allure-results")}"
    )

    @JvmField
    @RegisterExtension
    var selenoidAllure = Selenoid.Allure

    @BeforeEach
    fun beforeEach() {
        configure()
        Selenide.open(AuthProperties.baseurl)
    }

    @AfterEach
    open fun afterEach() {
        Selenide.closeWebDriver()
    }

    fun configure() {
        Configuration.browser = getProperty("leadium.selenide.browser", "chrome")
        if (getProperty("parallel", "false")!!.toBoolean())
            Configuration.browser = Selenoid::class.java.canonicalName
        Configuration.driverManagerEnabled =
            getProperty("leadium.selenide.driverManagerEnabled", "true")!!.toBoolean()
        Configuration.headless = getProperty("headless", "false")!!.toBoolean()
        Configuration.startMaximized = getProperty("leadium.selenide.startMaximized", "true")!!.toBoolean()
        Configuration.holdBrowserOpen = getProperty("leadium.selenide.holdBrowserOpen", "false")!!.toBoolean()
        Configuration.savePageSource = getProperty("leadium.selenide.savePageSource", "false")!!.toBoolean()
        Configuration.screenshots = getProperty("leadium.selenide.screenshots", "false")!!.toBoolean()
        Configuration.timeout = getProperty("leadium.selenide.timeout", "60000").toLong()
    }
}