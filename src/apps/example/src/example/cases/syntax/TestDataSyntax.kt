package example.cases.syntax

import example.TestDataMap

interface TestDataSyntax {

    val testData: TestDataMap

    fun searchRequest() = testData["searchRequest"]!!.left!!
    fun expectedResult() = testData["expectedResult"]!!.left!!
}