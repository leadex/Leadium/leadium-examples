package example.cases

import com.codeborne.selenide.Condition.*
import example.TestDataMap
import example.cases.syntax.TestDataSyntax
import example.steps.pages.DuckDuckGo
import example.steps.pages.Wikipedia
import org.junit.jupiter.api.Assertions
import org.leadium.core.TestCase
import org.leadium.core.minus
import org.leadium.report.allure.description.Description

@Description(
"""
Example Test Description
"""
)
class ExampleTest(override val testData: TestDataMap) : TestCase, TestDataSyntax {

    override fun begin() {

        DuckDuckGo() - {
            setSearchBox(searchRequest())
            enter()
            checkResult(expectedResult())
            clickResult()
        }
        Wikipedia() - {
            checkResult(expectedResult())
            checkLogoTitle()
            checkSearchInput()
            mathematics()
            mainPage()
            checkLogoCondition(be(not(focused)))
        }
    }
}