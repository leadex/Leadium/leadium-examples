package example

import org.leadium.core.AbstractProperties
import java.io.File
import java.lang.System.getProperty

object AuthProperties : AbstractProperties(File("${getProperty("rootProjectPath")}/res/auth.property")) {

    val baseurl = p.getProperty("baseurl")
}

object VideoProperties : AbstractProperties(File("${getProperty("rootProjectPath")}/src/apps/example/res/video.property")) {

    val enabled = p.getProperty("video.enabled")!!.toBoolean()
}